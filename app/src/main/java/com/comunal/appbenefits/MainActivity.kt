package com.comunal.appbenefits

import android.content.Context
import android.os.Bundle
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.comunal.appbenefits.ui.main.SectionsPagerAdapter
import androidx.core.view.ViewCompat.setAlpha
import androidx.core.app.ComponentActivity
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import com.comunal.appbenefits.ui.main.Benefits
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.io.IOException


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val jsonFileString = getJsonDataFromAsset(applicationContext, "data.json")
        Log.i("data", jsonFileString!!)
        val gson = Gson()
        val listPersonType = object : TypeToken<List<Benefits>>() {}.type
        val benefits: List<Benefits> = gson.fromJson(jsonFileString, listPersonType)
        benefits.forEachIndexed { idx, benefits -> Log.i("data", "> Item $idx:\n$benefits") }

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager, benefits)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)
        tabs.getTabAt(0)?.icon = ContextCompat.getDrawable(this,R.drawable.bienestar)
        tabs.getTabAt(1)?.icon = ContextCompat.getDrawable(this,R.drawable.diversion)
        tabs.getTabAt(2)?.icon = ContextCompat.getDrawable(this,R.drawable.aprendizaje)
        tabs.getTabAt(1)?.icon?.alpha = 80
        tabs.getTabAt(2)?.icon?.alpha = 80
        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {

            }

            override fun onPageSelected(position: Int) {
                when (position) {
                    0 -> {
                        tabs.getTabAt(0)?.icon?.alpha = 255
                        tabs.getTabAt(1)?.icon?.alpha = 80
                        tabs.getTabAt(2)?.icon?.alpha = 80
                    }
                    1 -> {
                        tabs.getTabAt(0)?.icon?.alpha = 80
                        tabs.getTabAt(1)?.icon?.alpha = 255
                        tabs.getTabAt(2)?.icon?.alpha = 80
                    }
                    2 -> {
                        tabs.getTabAt(0)?.icon?.alpha = 80
                        tabs.getTabAt(1)?.icon?.alpha = 80
                        tabs.getTabAt(2)?.icon?.alpha = 255
                    }
                }
            }

            override fun onPageScrollStateChanged(state: Int) {

            }
        })

    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }
}