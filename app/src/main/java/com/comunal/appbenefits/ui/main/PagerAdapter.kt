package com.comunal.appbenefits.ui.main

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.comunal.appbenefits.R

private val TAB_TITLES = arrayOf(
    R.string.tab_text_1,
    R.string.tab_text_2,
    R.string.tab_text_3
)

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
class SectionsPagerAdapter(private val context: Context, fm: FragmentManager,private val benefits: List<Benefits>) :
    FragmentPagerAdapter(fm) {

    private val WELFARE_FRAGMENT = 0
    private val LEARNING_FRAGMENT = 2
    private val FUN_FRAGMENT = 1

    override fun getItem(position: Int): Fragment {
        var fragment:Fragment ?= null

        when(position){
            WELFARE_FRAGMENT -> {
                fragment = WelFareFragment.newInstance(benefits[0])
            }
            FUN_FRAGMENT -> {
                fragment = FunFragment.newInstance(benefits[1])
            }
            LEARNING_FRAGMENT -> {
                fragment = LearningFragment.newInstance(benefits[2])
            }

        }
        return fragment!!
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return benefits[position].name
    }

    override fun getCount(): Int {
        // Show 3 total pages.
        return 3
    }
}