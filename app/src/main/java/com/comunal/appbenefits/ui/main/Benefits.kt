package com.comunal.appbenefits.ui.main

import java.io.Serializable

data class Benefits(
    val benefits: List<Benefit>,
    val name: String
):Serializable