package com.comunal.appbenefits.ui.main

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.comunal.appbenefits.R
/**
 * A placeholder fragment containing a simple view.
 */
class FunFragment : Fragment() {

    private var benefits:Benefits ?= null
    private var listBenefit:List<Benefit>? = arrayListOf()
    private var adapter: BenefitsAdapter ?= null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        benefits = arguments?.getSerializable(BENEFIT) as Benefits
        listBenefit = benefits?.benefits
        adapter = BenefitsAdapter(listBenefit!!,context!!)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_fun, container, false)
        val rvBenefit: RecyclerView = root.findViewById(R.id.rvBenefit)
        rvBenefit.layoutManager = LinearLayoutManager(context)
        rvBenefit.adapter = this.adapter
        return root
    }


    companion object {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private const val BENEFIT = "fun"

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        @JvmStatic
        fun newInstance(benefits: Benefits): FunFragment {
            return FunFragment().apply {
                arguments = Bundle().apply {
                    putSerializable(BENEFIT, benefits)
                }
            }
        }
    }
}