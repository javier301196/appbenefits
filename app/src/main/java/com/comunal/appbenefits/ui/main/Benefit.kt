package com.comunal.appbenefits.ui.main

import java.io.Serializable

data class Benefit(
    val business_name: String,
    val image: String,
    val name: String
) : Serializable