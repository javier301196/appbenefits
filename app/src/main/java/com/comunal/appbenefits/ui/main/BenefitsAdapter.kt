package com.comunal.appbenefits.ui.main

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.comunal.appbenefits.R
import kotlinx.android.synthetic.main.item_benefit.view.*

class BenefitsAdapter(
    private var operationSectionList: List<Benefit>,
    private var context: Context
) : RecyclerView.Adapter<BenefitsAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.item_benefit,
                parent, false
            )
        )
    }

    override fun getItemCount(): Int {
        return operationSectionList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val benefit = operationSectionList[position]
        holder.txtBenefitBody.text =  benefit.name
        holder.txtBenefitTitle.text =  benefit.business_name
        Glide.with(context)
            .load(benefit.image)
            .centerCrop()
            .placeholder(R.drawable.ic_launcher_foreground)
            .into(holder.imgBenefit)
    }


    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtBenefitBody: TextView = view.txtBenefitBody
        val txtBenefitTitle: TextView = view.txtBenefitTitle
        val imgBenefit: ImageView = view.imgBenefit
    }

}